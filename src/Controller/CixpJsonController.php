<?php

namespace Drupal\cixp_json\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * CIXP Json controller class.
 */

class CixpJsonController {

    public function renderJson(){
        return new JsonResponse(
                $this->getResults()
        );
    }

    protected function getResults() {
        $response = array();

        //TODO Change hardcoded data with configuration variables
        //Required
        $response['version'] = '0.7';
        //Required
        $response['timestamp'] = date( 'Y-m-d', time() ) . 'T' . date( 'H:i:s', time() ) . 'Z';
        //Required
        $response['ixp_list'] = array(
        array(
                'ixp_id'        => 1,
                'ixf_id'        => 7,
                'shortname'     => "CIXP",
                'name'          => "CERN Internet Exchange Point",
                'url'           => "https://cixp.net/",
                'country'       => 'CH',
                'support_email' => 'cixp-support@cern.ch',
                'vlan'          => $this->getVlanInformation(),
                'switch'        => $this->getSwitchInformation(),
        )
        );
        $response['member_list'] = $this->getMembersInformation();
    
        if(isset($_REQUEST['callback'])){
        $callback = check_plain($_REQUEST['callback']);
        }
        return $response;
    }

    protected function getVlanInformation() {
        $response = array();


        $nids = \Drupal::entityQuery('node')->condition('type','vlan')->condition('status', 1)->execute();
        foreach($nids as $nid) {
            $node =  \Drupal\node\Entity\Node::load($nid);
            //$ipv4_entity = node_load($node->field_ipv4[LANGUAGE_NONE][0]['target_id'], NULL, TRUE);
            $ipv4_entity = $node
            ->get("field_ipv4")
            ->first()
            ->get('entity')
            ->getTarget()
            ->getValue()
            ;
            //$ipv6_entity = node_load($node->field_ipv6[LANGUAGE_NONE][0]['target_id'], NULL, TRUE);
            $ipv6_entity = $node
            ->get("field_ipv6")
            ->first()
            ->get('entity')
            ->getTarget()
            ->getValue()
            ;
            //var_dump($node);
            $response[] = array(
                'id' => $node->get('field_ixp_id')->getValue(),
                'name' => $node->get('field_name')->getValue(),
                'ipv4' => array(
                    'prefix' => $ipv4_entity->get('field_prefix')->first()->getValue()['value'],
                    'mask_length' => (int)$ipv4_entity->get('field_mask_length')->first()->getValue()['value'],
                ),
                'ipv6' => array(
                    'prefix' => $ipv6_entity->get('field_prefix')->first()->getValue()['value'],
                    'mask_length' => (int)$ipv6_entity->get('field_mask_length')->first()->getValue()['value'],
                )
            );


        }
        return $response;
    }

    protected function getSwitchInformation() { 
        $response = array();

        $nids = \Drupal::entityQuery('node')->condition('type','switch')->condition('status', 1)->execute();
        foreach($nids as $nid){
            $node =  \Drupal\node\Entity\Node::load($nid);
            $response[] = array(
                'id'              => (int)$node->get('field_ixp_id')->first()->getValue()['value'],
                'name'            => $node->get('field_name')->first()->getValue()['value'],
                'colo'            => $node->get('field_colo')->first()->getValue()['value'],
                'city'            => $node->get('field_city')->first()->getValue()['value'],
                'country'         => $node->get('field_country')->first()->getValue()['value'],
                'pdb_facility_id' => (int)$node->get('field_pdb_facility_id')->first()->getValue()['value']
            );
        }
        return $response;

    }
  
    protected function getMembersInformation() {
        $response = array();

        $nids = \Drupal::entityQuery('node')->condition('type','participants')->condition('status', 1)->condition('field_as_number.value', 'NULL', '!=')->condition('field_visible.value', 1, '!=')->execute();

        foreach($nids as $nid){
            $node =  \Drupal\node\Entity\Node::load($nid);
            $connection_list = array(array(
                'ixp_id'    => 1,
                'state'     => 'active',
                'if_list'   => $this->getIfList($node),
                'vlan_list' => $this->getVlanList($node)
            ));
            $response[] = array_filter(array(
                'asnum'             => (int)$node->get('field_as_number')->first()->getValue()['value'],
                'url'               => $node->get('field_website')->value, // sometimes null so this works better that the previous code
                'name'              => $node->get('title')->first()->getValue()['value'],
                'contact_email'     => array_filter(array($node->get('field_noc_mail')->value), static function($var){return $var !== null;}),
                'connection_list'   => $connection_list,
            ), static function($var){return $var !== null;});

        }
        return $response;
    }

    protected function getIfList($node) {
        $response = array();

        $switch_entity = $node
            ->get("field_colo_location_")
            ->first()
            ->get('entity')
            ->getTarget()
            ->getValue()
            ;
        $switchId = -1; //Default value
        if($switch_entity){
            $switchId = $switch_entity->get('field_ixp_id')->value;
        }
        $response[] = array_filter(array(
                'switch_id' => $switchId,
                'if_speed'  => $node->get('field_if_speed')->value,
                'if_type'       => $node->get('field_if_type')->value
        ), static function($var){return $var !== null;});
        return $response;

    }

    protected function getVlanList($node) {
        $response = array();
        $response[] = array_filter(array(
                'vlan_id' => 100,  #Hardcoded value, only one vlan waiting for some entity reference in the participant
                'ipv4'  => array_filter(array(
                        'address' => $node->get('field_ipv4_address_192_65_185_0_')->value,
                        'as_macro' => $node->get('field_as_macro_ipv4')->value,
                        #'max_prefix' => (int),
                        'routeserver' => (boolean)$node->get('field_route_server')->value,
                        #'mac_addresses' => array_filter(array($node->field_mac_address[LANGUAGE_NONE][0]['value']), '_is_not_null'),
                ), static function($var){return $var !== null;}),
                'ipv6'  => array_filter(array(
                        'address' => $node->get('field_ipv6_address_2001_7f8_1c_2')->value,
                        'as_macro' => $node->get('field_as_macro_ipv6')->value,
                        #'max_prefix' => (int),
                        'routeserver' => (boolean)$node->get('field_route_server')->value,
                        #'mac_addresses' => array_filter(array($node->field_mac_address[LANGUAGE_NONE][0]['value']), '_is_not_null'),
                ), static function($var){return $var !== null;})
        ), static function($var){return $var !== null;});
        return $response;

    }


}